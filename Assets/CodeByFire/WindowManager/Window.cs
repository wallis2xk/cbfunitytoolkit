using UnityEngine;
using System.Collections;

namespace CodeByFire.UI {

    public class Window : MonoBehaviour {

        protected WindowManager parentManager;
        internal bool hasStarted = false;
        private WindowParams param;

        virtual protected void Awake() {
            if(WindowManager.currentManager != null) {
                parentManager = WindowManager.currentManager;
                parentManager.SetWindow(this);
            }
        }
        virtual protected void Start() {
            hasStarted = true;
        }

        virtual protected void Enable() {

        }
        virtual protected void Disable() {

        }

        void OnDestroy() {
            Disable();
        }

        virtual public void LoseFocus1() {

        }
        virtual public void GainFocus() {

        }
        virtual public void ShowWindow() {

        }

        public IEnumerator Open(WindowParams parameters) {
            param = parameters;
            yield return StartCoroutine(ProcessOpen(parameters));
        }
        virtual protected IEnumerator ProcessOpen(WindowParams parameters) {
            OnOpenComplete();
            yield break;
        }
        virtual protected void OnOpenComplete() {
            Enable();
        }

        public void SetCloseFunc(System.Action closeFunc) {
            if(param == null) param = new WindowParams();
            param.closeFunc = closeFunc;
        }
        protected void CloseSelf() {
            parentManager.CloseWindow(this);
        }

        internal IEnumerator Close() {
            Disable();
            yield return StartCoroutine(ProcessClose());
        }

        virtual protected IEnumerator ProcessClose() {
            yield break;
        }

        /// <summary>
        /// Called by the WindowManager as the last interaction with this window after closing. Process whether it has any last actions via param.closeFunc
        /// </summary>
        virtual public void FinaliseDestroy() {
            if(param != null && param.closeFunc != null) {
                param.closeFunc();
            }
            if(gameObject != null) {
                //hide so it doesn't flash because of destroying canvas group
                gameObject.SetActive(false);
                Destroy(gameObject);
            }
        }

    }

    public class WindowParams {

        public System.Action closeFunc;

    }

}

