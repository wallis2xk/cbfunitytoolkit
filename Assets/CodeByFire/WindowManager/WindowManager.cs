using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace CodeByFire.UI {

    public class WindowManager : MonoBehaviour {

        enum State { IDLE, LOADING, CLOSING, OPENING, EMPTY, PREPPING }

        public event System.Action<Window> OnWindowOpen;
        public event System.Action OnAllWindowsClosed;

        public Transform windowHolder;

        internal static WindowManager currentManager;

        Queue<WindowAction> onEmptyQueue = new Queue<WindowAction>();
        List<Window> currentWindows = new List<Window>();
        Window currentWindow;
        Window previousWindow;
        bool isRunning = false;

        State state;
        Queue<WindowAction> actionQueue = new Queue<WindowAction>();



        public static WindowManager instance {
            get {
                if(currentManager == null) {
                    GameObject obj = new GameObject("WindowManager");
                    currentManager = obj.AddComponent<WindowManager>();
                }
                return currentManager;
            }
        }
        void Awake() {
            currentManager = this;
        }

        /// <summary>
        /// Called by the Window being generated to let the manage know it exists
        /// </summary>
        /// <param name="window"></param>
        public void SetWindow(Window window) {
            currentWindow = window;
        }

        public void OpenWindow<T>() {
            OpenWindow<T>(null);
        }
        public void OpenWindow<T>(WindowParams parameters) {
            string prefab = typeof(T).Name;
            if(state == State.EMPTY || state == State.IDLE) {
                if(currentWindow != null) {
                    if(currentWindow.name == prefab) {
                        return;
                    }
                }
                if(currentWindow != null) {
                    WindowAction action = new WindowAction {
                        type = WindowAction.Type.CLOSE,
                        window = currentWindow,
                    };
                    actionQueue.Enqueue(action);
                }
                WindowAction action2 = new WindowAction {
                    type = WindowAction.Type.OPEN,
                    prefab = prefab,
                    parameters = parameters
                };
                actionQueue.Enqueue(action2);
                StartProcessActions();
            }
        }
        public void OpenWindowOverlay<T>() {
            OpenWindowOverlay<T>(null);
        }
        public void OpenWindowOverlay<T>(WindowParams parameters) {
            string prefab = typeof(T).Name;
            //if(state == WindowManagerState.EMPTY || state == WindowManagerState.IDLE) {
            WindowAction action = new WindowAction {
                type = WindowAction.Type.OPEN,
                prefab = prefab,
                parameters = parameters
            };
            actionQueue.Enqueue(action);
            
            if(state == State.EMPTY || state == State.IDLE) {
                StartProcessActions();
            }
        }

        public Window GetCurrentWindow() {
            return currentWindows.Count > 0 ? currentWindows[currentWindows.Count - 1] : null;
        }
        public List<Window> GetCurrentWindows() {
            return currentWindows;
        }

        public T FindWindow<T>() where T : Window {
            foreach(var win in currentWindows) {
                if(win.GetType() == typeof(T)) {
                    return win as T;
                }
            }
            return null;
        }

        private void StartProcessActions() {
            if(isRunning) return;
            SetState(State.PREPPING);
            StartCoroutine(ProcessActions());
        }
        private IEnumerator ProcessActions() {
            isRunning = true;
            while(actionQueue.Count > 0) {
                WindowAction currentAction = actionQueue.Dequeue();
                switch(currentAction.type) {
                    case WindowAction.Type.CLOSE:
                        currentWindows.Remove(currentAction.window);
                        SetState(State.CLOSING);
                        yield return StartCoroutine(currentAction.window.Close());
                        previousWindow = currentWindow;
                        currentWindow = null;
                        if(onEmptyQueue.Count > 0) {
                            actionQueue.Enqueue(onEmptyQueue.Dequeue());
                        }
                        if(currentWindows.Count > 0) {
                            currentWindow = currentWindows[currentWindows.Count - 1];
                            currentWindow.GainFocus();
                            SetState(State.IDLE);
                        } else {
                            SetState(State.EMPTY);
                        }
                        previousWindow.FinaliseDestroy();
                        if(actionQueue.Count == 0) {
                            //hmm, why must the action count be 0?
                            //previousWindow.FinaliseDestroy();
                            if(currentWindows.Count == 0) {
                                if(OnAllWindowsClosed != null) OnAllWindowsClosed();
                            }
                        }
                        break;
                    case WindowAction.Type.OPEN:
                        //disable clicks
                        LoseFocus();
                        yield return StartCoroutine(LoadWindow(currentAction));

                        currentWindow.ShowWindow();

                        currentWindows.Add(currentWindow);

                        if(OnWindowOpen != null) OnWindowOpen(currentWindow);

                        break;
                }
            }
            isRunning = false;
        }

        public void LoseFocus() {
            currentWindows.ForEach(delegate (Window win) {
                win.LoseFocus1();
            });
        }
        public void GainFocus() {
            if(currentWindow != null) currentWindow.GainFocus();
        }

        private IEnumerator LoadWindow(WindowAction action) {
            SetState(State.LOADING);
            currentManager = this;
            currentWindow = null;
            var windowGO = Instantiate(Resources.Load("UI/Windows/" + action.prefab)) as GameObject;
            windowGO.SetActive(true);

            //wait for the Window to load and poss its reference back to this WindowManager
            while(currentWindow == null) {
                yield return null;
            }

            SetState(State.OPENING);

            currentWindow.transform.SetParent(windowHolder);
            currentWindow.GetComponent<RectTransform>().sizeDelta = Vector2.zero;
            currentWindow.GetComponent<RectTransform>().localScale = Vector3.one;
            Vector3 pos = Vector3.zero;
            pos.z = -1f - ((currentWindows.Count - 1) * 2f);
            currentWindow.transform.localPosition = pos;

            while(!currentWindow.hasStarted) {
                yield return null;
            }
            yield return StartCoroutine(currentWindow.Open(action.parameters));
            SetState(State.IDLE);
        }

        public void CloseWindow() {
            if(currentWindow != null) {
                CloseWindow(currentWindow);
            }
        }
        public void CloseWindow(Window window) {
            CloseWindow(window, true);
        }
        private void CloseWindow(Window window, bool isAutoStart) {
            WindowAction action = new WindowAction {
                type = WindowAction.Type.CLOSE,
                window = window,
            };
            actionQueue.Enqueue(action);
            if(isAutoStart) {
                StartProcessActions();
            }
        }
        public void CloseAllWindows() {
            for(int i = currentWindows.Count - 1; i >= 0; i--) {
                CloseWindow(currentWindows[i], i == currentWindows.Count - 1);
            }
        }

        private void SetState(State state) {
            this.state = state;
            //globalState = state;
        }

    }

    public class WindowAction {

        public enum Type { OPEN, CLOSE }

        public Type type;
        public string prefab;
        public Window window;
        public WindowParams parameters;

    }

}
