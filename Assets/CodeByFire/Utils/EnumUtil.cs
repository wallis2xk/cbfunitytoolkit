using System;
using System.Collections;
using System.Collections.Generic;

public static class EnumUtil {

    public static T ParseEnum<T>(string value) {
        return (T)Enum.Parse(typeof(T), value, true);
    }

    public static IEnumerable<T> GetValues<T>() {
        return (T[])Enum.GetValues(typeof(T));
    }

}