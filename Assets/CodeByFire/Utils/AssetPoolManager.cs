using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using CodeByFire.Utils;
using System;

public class AssetPoolManager : ResourceSingleton<AssetPoolManager> {

    [System.Serializable]
    public class AssetListItem {
        public AssetPoolItem prefab;
        public int initialCount;
    }
    [System.Serializable]
    public class AssetItemPool<T> where T : AssetPoolItem {
        public T prefab;
        public int initialCount;
        public List<T> availablePool = new List<T>();
        public List<T> takenPool = new List<T>();
    }

    public List<AssetListItem> items;
    public Dictionary<Type, AssetItemPool<AssetPoolItem>> poolDict = new Dictionary<Type, AssetItemPool<AssetPoolItem>>();


    public void CreateCache() {
        if(poolDict.Count > 0) throw new Exception("Cache has already been created");

        var len = items.Count;
        for(int i=0; i< len; i++) {
            var item = items[i];
            var itemType = item.prefab.GetType();

            var itemPool = new AssetItemPool<AssetPoolItem>();
            for(int j = 0; j < item.initialCount; j++) {
                var go = this.CustomInstantiate(item.prefab, false);
                go.transform.SetParent(transform);
                itemPool.availablePool.Add(go);
            }

            poolDict.Add(itemType, itemPool);
        }
    }

    public T Get<T>() where T : AssetPoolItem {
        var pool = poolDict[typeof(T)];
        T item = (T) pool.availablePool[0];
        pool.availablePool.RemoveAt(0);
        pool.takenPool.Add(item);
        return item;
    }

    public void Reset<T>(T item) where T : AssetPoolItem {
        var pool = poolDict[item.GetType()];
        ResetItem(item);
        pool.takenPool.Remove(item);
        pool.availablePool.Add(item);
    }

    public void ResetItems<T>() where T : AssetPoolItem {
        var pool = poolDict[typeof(T)];
        var i = 0;
        var len = pool.takenPool.Count;
        while(i < len) {
            var item = pool.takenPool[i];
            ResetItem(item);
            pool.availablePool.Add(item);
            i++;
        }
        pool.takenPool.Clear();
    }

    void ResetItem(AssetPoolItem item) {
        item.Deactivate();
        item.transform.SetParent(transform);
    }

    public void ResetEverything() {
        foreach(var entry in poolDict) {
            var i = 0;
            var len = entry.Value.takenPool.Count;
            while(i < len) {
                var item = entry.Value.takenPool[i];
                ResetItem(item);
                entry.Value.availablePool.Add(item);
                i++;
            }
            entry.Value.takenPool.Clear();
        }
    }

}