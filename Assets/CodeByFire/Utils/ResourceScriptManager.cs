using System;
using System.Collections.Generic;
using UnityEngine;

public class ResourceScriptManager
{
	private static Dictionary<Type, MonoBehaviour> instances = new Dictionary<Type, MonoBehaviour>();
	
	public static T GetInstance<T> () where T : MonoBehaviour {
		return GetInstance<T> (true);
	}
	
	public static T GetInstance<T> (bool createInstance) where T : MonoBehaviour {
		Type type = typeof(T);

        MonoBehaviour existingInstance;
		if (instances.TryGetValue (type, out existingInstance)) {
			return existingInstance as T;
		}

		T newInstance;
		if (createInstance) {
			newInstance = (GameObject.Instantiate (Resources.Load (type.Name)) as GameObject).GetComponent<T> ();
		} else {
			newInstance = (Resources.Load (type.Name) as GameObject).GetComponent<T> ();
		}
		instances.Add (type, newInstance);
		return newInstance;
	}
}

