using UnityEngine;
using System.Text;

public static class ColorExt
{
	public static string ToHex(this Color color) {
        string hex = color.r.ToString("X2") + color.g.ToString("X2") + color.b.ToString("X2");
        return hex;
    }
	
}
