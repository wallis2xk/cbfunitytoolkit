using UnityEngine;
using System.Collections;

namespace CodeByFire.Utils {

    public class LogReporter : ResourceSingleton<LogReporter> {

        public bool isEnabled = false;

        public bool isEditorEnabled = false;
        public bool isDevelopmentBuildEnabled = true;
        public bool isProductionBuildEnabled = false;
        public bool doesSendDuplicate = true;
        public float duplicateSecondsGap = 60f;
        public string slackChannel = "";

        string previousErrorStackTrace = "";
        float previousErrorTime = 0f;

        public void Init() {
            
        }

        void OnEnable() {
            if(isEnabled) Application.logMessageReceivedThreaded += Application_logMessageReceivedThreaded;
        }
        void OnDisable() {
            if(isEnabled) Application.logMessageReceivedThreaded -= Application_logMessageReceivedThreaded;
        }

        private void Application_logMessageReceivedThreaded(string condition, string stackTrace, LogType type) {
            if(Application.isEditor && !isEditorEnabled) return;
            if(Debug.isDebugBuild && !isDevelopmentBuildEnabled) return;
            if(!Debug.isDebugBuild && !isProductionBuildEnabled) return;

            if(type == LogType.Exception) {
                if(stackTrace != previousErrorStackTrace || (Time.time - previousErrorTime) > duplicateSecondsGap) {
                    if(slackChannel.Length > 0) {
                        SlackPoster.Instance.PostToSlack(slackChannel, condition + "\n" + stackTrace);
                    } else {
                        SlackPoster.Instance.PostToSlack(condition + "\n" + stackTrace);
                    }

                    previousErrorStackTrace = stackTrace;
                    previousErrorTime = Time.time;
                }
            }
        }

        [RuntimeInitializeOnLoadMethod]
        static void OnInit() {
            Instance.Init();
        }

    }

}
