using UnityEngine;

/// <summary>
/// Be aware this will not prevent a non singleton constructor
///   such as `T myT = new T();`
/// To prevent that, add `protected T () {}` to your singleton class.
/// 
/// As a note, this is made as MonoBehaviour because we need Coroutines.
/// </summary>
/// 

public class ResourceSingleton<T> : MonoBehaviour where T : MonoBehaviour {

    private static T _instance;
    private static bool isApplicationIsQuitting = false;
    private static object _lock = new object();

    public static T Instance {
        get {
            if(isApplicationIsQuitting) {
                return null;
            }

            lock (_lock) {
                if(_instance == null) {
                    if(_instance == null) {
                        _instance = ResourceScriptManager.GetInstance<T>(Application.isPlaying);

                        _instance.gameObject.name = typeof(T).ToString();

                        DontDestroyOnLoad(_instance.gameObject);
                    }
                }

                return _instance;
            }
        }
    }

    public void OnDestroy() {
        isApplicationIsQuitting = true;
    }
}
