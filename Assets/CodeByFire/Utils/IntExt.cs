using UnityEngine;
using System.Text;

public static class IntExt
{
	public static string FormatTimeLeft(this int secondsLeft) {
		if(secondsLeft <= 60) {
			return secondsLeft + " sec";
		}else if(secondsLeft < 60*60) {
			int minutesLeft = Mathf.FloorToInt(secondsLeft / 60);
			return minutesLeft + "m" + ((secondsLeft % 60) > 0 ? (" " + (secondsLeft % 60) + "s") : "");
		}else if(secondsLeft < 60*60*24) {
			int minutesLeft = Mathf.FloorToInt(secondsLeft / 60) % 60;
			int hoursLeft = Mathf.FloorToInt(secondsLeft / (60*60));
			return hoursLeft + "h" + (minutesLeft > 0 ? (" " + minutesLeft + "m") : "");
		}else {
			int hoursLeft = Mathf.FloorToInt(secondsLeft / (60*60)) % 24;
			int daysLeft = Mathf.FloorToInt(secondsLeft / (60*60*24));
			return daysLeft + "d" + (hoursLeft > 0 ? (" " + hoursLeft + "h") : "");
		}
	}

	public static string FormatTimeLeftShort(this int secondsLeft) {
		if(secondsLeft <= 60) {
			return secondsLeft + "s";
		}else if(secondsLeft < 60*60) {
			int minutesLeft = Mathf.FloorToInt(secondsLeft / 60);
			return minutesLeft + "m" + ((secondsLeft % 60) > 0 ? ("" + (secondsLeft % 60) + "s") : "");
		}else {
			int minutesLeft = Mathf.FloorToInt(secondsLeft / 60) % 60;
			int hoursLeft = Mathf.FloorToInt(secondsLeft / (60*60));
			return hoursLeft + "h" + (minutesLeft > 0 ? ("" + minutesLeft + "m") : "");
		}
	}

	public static string FormatTimeLeftOneUnit(this int secondsLeft) {
		if(secondsLeft <= 180) {
			return secondsLeft + "s";
		}else if(secondsLeft < 180*60) {
			int minutesLeft = Mathf.FloorToInt(secondsLeft / 60);
			return minutesLeft + "m";
		}else {
			int minutesLeft = Mathf.FloorToInt(secondsLeft / 60) % 60;
			int hoursLeft = Mathf.FloorToInt(secondsLeft / (60*60));
			return hoursLeft + "h";
		}
	}

	public static string FormatTimeFull(this int seconds) {
		if(seconds <= 60) {
			return seconds + " seconds";
		}else if(seconds < 60*60) {
			int minutesLeft = Mathf.FloorToInt(seconds / 60);
			return minutesLeft + " minutes " + ((seconds % 60) > 0 ? ("" + (seconds % 60) + " seconds") : "");
		}else {
			int minutes = Mathf.FloorToInt(seconds / 60) % 60;
			int hours = Mathf.FloorToInt(seconds / (60*60));
			return hours + " hours " + (minutes > 0 ? ("" + minutes + " minutes") : "");
		}
	}

	public static string FormatTimer(this int secondsLeft) {
		if(secondsLeft <= 60) {
			return secondsLeft.ToString();
		}else {
			int minutesLeft = Mathf.FloorToInt(secondsLeft / 60);
			int secondsLeft2 = (secondsLeft % 60);
			return minutesLeft + ":" + (secondsLeft2 < 10 ? "0" : "") + secondsLeft2;
		}
	}

    public static string FormatTimeSingle(this double seconds) {
        if (seconds <= 60) {
            return Mathf.FloorToInt((float)seconds) + " second" + (seconds != 1 ? "s" : "");
        } else if (seconds < 60 * 60) {
            int minutes = Mathf.FloorToInt((float)seconds / 60f);
            return minutes + " minute" + (minutes != 1 ? "s" : "");
        } else if (seconds < 60 * 60 * 24) {
            int hours = Mathf.FloorToInt((float)seconds / (60f * 60f));
            return hours + " hour" + (hours != 1 ? "s" : "");
        } else {
            int days = Mathf.FloorToInt((float)seconds / (60f * 60f * 24f));
            return days + " day" + (days != 1 ? "s" : "");
        }
    }

}
