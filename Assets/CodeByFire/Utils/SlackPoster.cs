using UnityEngine;
using System.Collections;

namespace CodeByFire.Utils {

    public class SlackPoster : ResourceSingleton<SlackPoster> {

        public bool isActive = true;
        public string slackURL = "";

        public delegate string GetUserInfo();
        GetUserInfo getUserInfo;

        public void SetGetUserInfo(GetUserInfo getUserInfo) {
            this.getUserInfo = getUserInfo;
        }

        string _GetUserInfo() {
            if (getUserInfo != null) return getUserInfo();
            return "User";
        }

        public void PostToSlack(string body) {
            PostToSlack(_GetUserInfo(), "", body);
        }
        public void PostToSlack(string channel, string body) {
            PostToSlack(_GetUserInfo(), channel, body);
        }
        public void PostToSlack(string user, string channel, string body) {
            if (!isActive) return;

            WWWForm form = new WWWForm();
            var payload = "{\"username\":\"" + user.Replace("'", @"\'") + "\", \"text\":\"" + body.Replace("'", @"\'") + "\"" + (channel.Length > 0 ? ", \"channel\":\"" + channel + "\"" : "") + "}";
            form.AddField("payload", payload);
            WWW www = new WWW(slackURL, form);
            StartCoroutine(DoRequest(www));
        }

        IEnumerator DoRequest(WWW www) {
            yield return www;
        }

    }

}
