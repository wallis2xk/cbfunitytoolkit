using UnityEngine;
using System.Collections;

public class AssetPoolItem : MonoBehaviour {

    public void Activate(Transform parent) {
        transform.SetParent(parent);
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;
        transform.localScale = Vector3.one;

        Activate();
    }
	virtual public void Activate() {
        gameObject.SetActive(true);
    }

    virtual public void Deactivate() {
        gameObject.SetActive(false);
    }

}
