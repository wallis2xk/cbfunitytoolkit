using UnityEngine;
using System.Text;

public static class StringExt {
#if UNITY_WINRT
	private static MD5.MD5 md = new MD5.MD5();
#endif

    public static string MD5(this string strToEncrypt) {
        //Debug.LogError("--str:" + strToEncrypt);
        //strToEncrypt = "abc";
#if UNITY_WINRT
		md.Value = strToEncrypt;
		return md.FingerPrint.ToLower();
#else
        System.Text.UTF8Encoding ue = new System.Text.UTF8Encoding();
        byte[] bytes = ue.GetBytes(strToEncrypt);
        System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
        byte[] hashBytes = md5.ComputeHash(bytes);
        string hashString = "";
        for(int i = 0; i < hashBytes.Length; i++) {
            hashString += System.Convert.ToString(hashBytes[i], 16).PadLeft(2, '0');
        }
        return hashString.PadLeft(32, '0');
#endif
    }

    static public string EncodeTo64(this string toEncode) {
        byte[] toEncodeAsBytes = System.Text.UTF8Encoding.UTF8.GetBytes(toEncode);
        string returnValue = System.Convert.ToBase64String(toEncodeAsBytes);
        return returnValue;
    }

    static public string DecodeFrom64(this string encodedData) {
        byte[] encodedDataAsBytes = System.Convert.FromBase64String(encodedData);
        string returnValue = System.Text.UTF8Encoding.UTF8.GetString(encodedDataAsBytes);
        return returnValue;
    }

    /*public static string SHA1 (this string strToEncrypt)
	{
		System.Text.UTF8Encoding ue = new System.Text.UTF8Encoding ();
		byte[] bytes = ue.GetBytes (strToEncrypt);
		System.Security.Cryptography.SHA1 sha1 = new System.Security.Cryptography.SHA1CryptoServiceProvider ();
		byte[] hashBytes = sha1.ComputeHash (bytes);
		string hashString = "";
		for (int i = 0; i < hashBytes.Length; i++) {
			hashString += System.Convert.ToString (hashBytes[i], 16).PadLeft (2, '0');
		}
		return hashString.PadLeft (40, '0');
	}*/

    public static string ZeroPadLeft(this string target, int count) {
        while(target.Length < count) target = "0" + target;
        return target;
    }

    public static string ThousandSeperate(this int target) {
        return System.String.Format("{0:#,0}", target);
    }

    public static string ThousandSeperate(this float target) {
        return System.String.Format("{0:#,0}", target);
    }

    public static string ToFirstUpper(this string str) {
        if(str == null) return null;

        if(str.Length > 1)
            return char.ToUpper(str[0]) + str.Substring(1);

        return str.ToUpper();
    }

    public static string Wordify(this string pascalCaseString) {
        pascalCaseString = System.Text.RegularExpressions.Regex.Replace(pascalCaseString, @"(?<a>(?<!^)((?:[A-Z][a-z])|(?:(?<!^[A-Z]+)[A-Z0-9]+(?:(?=[A-Z][a-z])|$))|(?:[0-9]+)))", @" ${a}");
        if(pascalCaseString.Length > 1) {
            pascalCaseString = pascalCaseString.Substring(0, 1).ToUpper() + pascalCaseString.Substring(1);
        }
        pascalCaseString = pascalCaseString.Replace("_", " ");
        return pascalCaseString;
    }

    public static string Wrap(this string text, int width) {
        int pos;
        int next;

        StringBuilder sb = new StringBuilder();

        // Lucidity check
        if(width < 1)
            return text;

        // Parse each line of text
        for(pos = 0; pos < text.Length; pos = next) {
            // Find end of line
            int eol = text.IndexOf("\n", pos);
            if(eol == -1) {
                next = eol = text.Length;
            } else {
                next = eol + 1;
            }

            // Copy this line of text, breaking into smaller lines as needed
            if(eol > pos) {
                do {
                    int len = eol - pos;
                    if(len > width) {
                        len = BreakLine(text, pos, width);
                    }

                    sb.Append(text, pos, len);
                    sb.Append("\n");

                    // Trim whitespace following break
                    pos += len;
                    while(pos < eol && System.Char.IsWhiteSpace(text[pos])) {
                        pos++;
                    }

                } while(eol > pos);
            } else {
                sb.Append("\n"); // Empty line
            }
        }
        string done = sb.ToString();
        return done.EndsWith("\n") ? done.Substring(0, done.Length - 1) : done;
    }

    public static string DecodeHtmlChars(this string aText) {
        string[] parts = aText.Split(new string[] { "&#x" }, System.StringSplitOptions.None);
        for(int i = 1; i < parts.Length; i++) {
            int n = parts[i].IndexOf(';');
            string number = parts[i].Substring(0, n);
            try {
                int unicode = System.Convert.ToInt32(number, 16);
                parts[i] = ((char)unicode) + parts[i].Substring(n + 1);
            } catch { }
        }
        return System.String.Join("", parts);
    }

    private static int BreakLine(string text, int pos, int max) {
        // Find last whitespace in line
        int i = max;
        while(i >= 0 && !System.Char.IsWhiteSpace(text[pos + i]))
            i--;

        // If no whitespace found, break at maximum length
        if(i < 0)
            return max;

        // Find start of whitespace
        while(i >= 0 && System.Char.IsWhiteSpace(text[pos + i]))
            i--;

        // Return length of text before whitespace
        return i + 1;
    }

    public static Color ToColor(this string hex) {
        byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
        byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
        byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
        return new Color(r, g, b);
    }

}
